import {Brain} from './brain'

const dataTraining = [
  {
    input: [0,0,0],
    output: [0,0,0,0,0,0,0]
  },
  {
    input: [1,0,0],
    output: [1,0,0,0,0,0,0]
  },
  {
    input: [0.5,0,0],
    output: [0.5,0,0,0,0,0,0]
  },
  {
    input: [0,1,0],
    output: [0,1,0,0,0,0,0]
  },
  {
    input: [0,0.5,0],
    output: [0,0.5,0,0,0,0,0]
  },
  {
    input: [1,1,0],
    output: [0,0,1,0,0,0,0]
  },
  {
    input: [0,0,1],
    output: [0,0,0,1,0,0,0]
  },
  {
    input: [1,0,1],
    output: [0,0,0,0,1,0,0]
  },
  {
    input: [0,1,1],
    output: [0,0,0,0,0,1,0]
  },
  {
    input: [1,1,1],
    output: [0,0,0,0,0,0,1]
  }
];

const dataTest = [
  {
    input: [0.5,0.5,0],
    output: [0,0,1,0,0,0,0]
  },
  {
    input: [0.5,0.5,0.5],
    output: [0,0,0,0,0,0,1]
  }
];


async function DoStuff() {
  let brain = new Brain(dataTraining[0].input.length, dataTraining[0].output.length);
  if(!brain.loadKoeffFromFile('./model.json')){
    await brain.learn(dataTraining, 10000);
    brain.saveKoeffsToFile('./model.json');
  }

  dataTraining.forEach(point => {
    let values = brain.process(point.input);
    console.log(`Input ${point.input}`);
    console.log(`Expectation ${point.output.map(v => v.toFixed(2)).join(', ')}`);
    console.log(`prediction: ${values.map(v => v.toFixed(2)).join(', ')}`);
    console.log(``);
  });

  dataTest.forEach(point => {
    let values = brain.process(point.input);
    console.log(`Input ${point.input}`);
    console.log(`Expectation ${point.output.map(v => v.toFixed(2)).join(', ')}`);
    console.log(`prediction: ${values.map(v => v.toFixed(2)).join(', ')}`);
    console.log(``);
  });
}

DoStuff();
