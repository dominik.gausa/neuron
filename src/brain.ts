import * as tf from '@tensorflow/tfjs';
import * as fs from 'fs';

export class Brain {
  inputs: number;
  outputs: number;
  model : tf.Sequential;

  constructor(inputs: number, outputs: number){
    this.inputs = inputs;
    this.outputs = outputs;

    this.createModel();
  }

  createModel(){
    this.model = tf.sequential({
      layers: [
        tf.layers.dense({inputShape: [this.inputs], units: 4 * this.inputs, activation: 'relu'}),
        tf.layers.dense({units: 4 * this.inputs, activation: 'relu'}),
        tf.layers.dense({units: this.outputs, activation: 'softmax'}),
      ]
    });

    this.model.compile({optimizer: 'sgd', loss: 'meanSquaredError'});
  }

  async learn(data: {input: number[], output: number[]}[], epochs: number): Promise<{
      inOffset: number, inGain: number, outOffset: number, outGain: number
    }>
  {
    let xs = tf.tensor(data.map(item => item.input));
    let ys = tf.tensor(data.map(item => item.output));

    const factors = {
      inOffset: xs.min().dataSync()[0],
      inGain: xs.max().sub(xs.min()).dataSync()[0],
      outOffset: ys.min().dataSync()[0],
      outGain: ys.max().sub(ys.min()).dataSync()[0]
    };

    xs = xs.sub(xs.min());
    xs = xs.div(xs.max());
    ys = ys.sub(ys.min());
    ys = ys.div(ys.max());
    //console.log(`Normalized xs ${xs}`);
    //console.log(`Normalized ys ${ys}`);
    
    const epochChunks = 100;
    for(let epoch = 0; epoch < epochs; epoch += epochChunks)
    {
      let info = await this.model.fit(xs, ys, {
        epochs: epochChunks,
        callbacks: {
          //onEpochEnd: (epoch, log) => console.log(`Epoch ${epoch}: loss = ${log ? log.loss : '?'}, Accuracy = ${JSON.stringify(log)}`)
        }
      });
      //console.log('Final accuracy', JSON.stringify(info));
      console.log(`Epoch ${epoch}`); //: loss: ${info.params.history.loss.slice(-1)}`);
    }
    console.log(`Training done!`);

    return factors;
  }

  loadKoeff(layers: number[][])
  {
    this.model.layers.forEach((layer, idxLayer) => {
      layer.weights.forEach((w, idx) => {
        let layerWeights = layers.shift();
        w.write(tf.reshape(layerWeights, w.shape));
        //console.log(`layer: ${idxLayer}   idx: ${idx}   size: ${layerWeights.length}`);
        //console.log(`layer: ${idxLayer}   idx: ${idx}   weight: ${layerWeights.map(v => v.toFixed(3)).join(', ')}`);
      });
    });
  }

  dumpKoeff(): number [][]
  {
    let layers = [];
    this.model.layers.forEach((layer, idxLayer) => {
      layer.weights.forEach((w, idx) => {
        layers.push(w.read().dataSync());
        //console.log(`layer: ${idxLayer}   idx: ${idx}   size: ${layers[layers.length-1].length}`);
      });
    });

    return layers;
  }

  randomizeKoeffs(factor: number)
  {
    let koeffs = this.dumpKoeff();
    let koeffsNew = koeffs.map(layer =>
      layer.map(k => {
        let moreOrLessGaussian = Math.tanh((Math.random()*2-1) * factor)
        return k + moreOrLessGaussian;
      }));
    this.loadKoeff(koeffsNew);
  }

  loadKoeffFromFile(file: string): boolean
  {
    try{
      console.log(`Load from File: '${file}'`);
      
      let data = fs.readFileSync(file, 'utf8');
      let layers = JSON.parse(data) as number[][];
      this.loadKoeff(layers);
  
      console.log(`Try to load precompiled   Done!`);
      return true;
    }catch(ex){
      console.log(ex);
      return false;
    }
  }
  
  saveKoeffsToFile(file: string){
    console.log(`Saving to File: '${file}'`);
    let layers = this.dumpKoeff();
    let layerArray = layers.map(layer => Object.values(layer));
    fs.writeFileSync(file, JSON.stringify(layerArray, null, 2));
  }

  process(inputs: number[]): number[]
  {
    let prediction = this.model.predict(tf.tensor2d([inputs]), {batchSize: 1})  as tf.Tensor;
    let values = Array.from(prediction.div(prediction.max()).dataSync());
    return values;
  }

  mutate(learnRate: number): Brain
  {
    let b = new Brain(this.inputs, this.outputs);
    b.loadKoeff(this.dumpKoeff());
    b.randomizeKoeffs(learnRate);
    return b;
  }
}


